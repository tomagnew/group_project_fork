var assert = require('chai').assert;  // use chai 'assert' assertion library
import $ from 'jquery';
import member_area from '../index';

describe('Member Area module integration tests', () => {
    // arrange
    let membersArea = $("<div>", { id: 'member_area' }).css("color", "red");
    membersArea.html("<h1>Content in Members Area<h1>").hide();
    $('body').append(membersArea);

    // act
    member_area({ // simulate access
        loggedIn: true,
        currentUser: {
            name: "anyone"
        }
    });

    // assert
    it('should show stuff in "Members" area if user logged in', function (done) {
        this.timeout(1600);
        setTimeout(() => {
            let visible = $('#member_area').is(':visible');
            console.log("div#member_area: " + visible);
            assert.equal(visible, true);
            done();
        }, 1500);
    });

    // wait 3 seconds vefore running the next test

    it('should hide stuff in "Members" area if user not logged in', function (done) {
        this.timeout(2600);
        setTimeout(() => {
            // arrange
            $('#member_area').show();
            member_area(false); // simulate logged in
            setTimeout(() => {
                let visible = $('#member_area').is(':visible');
                console.log("div#member_area: " + visible);
                assert.equal(visible, false);
                done();
            }, 1000);
        }, 1500);
    });


});